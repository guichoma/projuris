package br.com.projuris;

import java.util.Arrays;

public class MyFindArray implements FindArray {

    public int findArray(int[] array, int[] subArray) {

        boolean subArrayValidation;
        int maxArrayPosition = array.length - subArray.length;

        int result = -1;

        for (int i = 0; i <= maxArrayPosition; i++) {
            subArrayValidation = true;

            for (int j = 0; j < subArray.length; j++) {
                if(subArray[j] != array[i + j]) {
                    subArrayValidation = false;
                    break;
                }
            }

            if(subArrayValidation) {
                result = i;
            }

        }

        System.out.println("array: " + Arrays.toString(array));
        System.out.println("subArray:" + Arrays.toString(subArray));
        System.out.println("result: " + result);
        System.out.println("-----------------------------------");

        return result;

    }

}
