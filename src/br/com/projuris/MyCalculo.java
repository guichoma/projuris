package br.com.projuris;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.math.BigDecimal;
import java.util.Map;


public class MyCalculo implements Calculo {

    @Override
    public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {
        Map<String, BigDecimal> cargosPorCusto = new HashMap<String, BigDecimal>();

        for (Funcionario funcionario : funcionarios) {
            String cargo = funcionario.getCargo();
            BigDecimal salario = funcionario.getSalario().setScale(2, BigDecimal.ROUND_HALF_EVEN);

            if(cargosPorCusto.get(funcionario.getCargo()) == null) {
                cargosPorCusto.put(cargo, salario);
            } else {
                cargosPorCusto.put(cargo, cargosPorCusto.get(cargo).add(salario));
            }
        }

        System.out.println("cargosPorCusto: " + cargosPorCusto);
        System.out.println("-----------------------------------");

        return null;
    }

    @Override
    public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {
        Map<String, BigDecimal> departamentoPorCusto = new HashMap<String, BigDecimal>();

        for (Funcionario funcionario : funcionarios) {
            String departamento = funcionario.getDepartamento();
            BigDecimal salario = funcionario.getSalario().setScale(2, BigDecimal.ROUND_HALF_EVEN);

            if(departamentoPorCusto.get(funcionario.getDepartamento()) == null) {
                departamentoPorCusto.put(departamento, salario);
            } else {
                departamentoPorCusto.put(departamento, departamentoPorCusto.get(departamento).add(salario));
            }
        }

        System.out.println("departamentoPorCusto: " + departamentoPorCusto);
        System.out.println("-----------------------------------");

        return null;
    }
}
