package br.com.projuris;

public class MyFindChar implements FindCharachter {

    public char findChar(String word) {

        for (int i = 0; i < word.length() ; i++) {
            boolean letterNotRepeated = true;
            for (int j = 0; j < word.length(); j++) {

                if(word.toUpperCase().charAt(i) == word.toUpperCase().charAt(j) && i != j) {
                    letterNotRepeated = false;
                    break;
                }
            }

            if(letterNotRepeated) {
                System.out.println("Palavra: " + word);
                System.out.println("Primeira letra não repetida: " + word.charAt(i));
                System.out.println("-----------------------------------");

                return word.charAt(i);
            }
        }

        return ' ';
    }
}
