package br.com.projuris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        MyFindArray myFindArray = new MyFindArray();
        System.out.println("1ª Questão");
        System.out.println("-----------------------------------");
        myFindArray.findArray(new int[]{4, 9, 3, 7, 8}, new int[]{3, 7});
        myFindArray.findArray(new int[]{1, 3, 5}, new int[]{1});
        myFindArray.findArray(new int[]{7, 8, 9}, new int[]{8, 9, 10});
        myFindArray.findArray(new int[]{4, 9, 3, 7, 8, 3, 7, 1},  new int[]{3, 7});
        myFindArray.findArray(new int[]{1, 5, 8, 7, 3, 5, 3, 5},  new int[]{3, 5});
        myFindArray.findArray(new int[]{1, 2, 3, 3, 3, 3, 3, 3},  new int[]{3});

        System.out.println("2ª Questão");
        System.out.println("-----------------------------------");
        MyFindChar myFindChar = new MyFindChar();
        myFindChar.findChar("Teste");
        myFindChar.findChar("Desafio");
        myFindChar.findChar("Palavra");
        myFindChar.findChar("Repetida");

        System.out.println("3ª Questão");
        System.out.println("-----------------------------------");
        //4.101.8
        Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
        Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
        Funcionario funcionario8 = new Funcionario("Assistente", "Jurídico", new BigDecimal(1800.90));

        //24.001,2
        Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
        Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
        Funcionario funcionario9 = new Funcionario("Gerente", "Jurídico", new BigDecimal(9500.50));

        //34.000,45
        Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
        Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
        Funcionario funcionario10 = new Funcionario("Diretor", "Jurídico", new BigDecimal(13000.0));

        //700,4
        Funcionario funcionario7 = new Funcionario("Estagiário", "Jurídico", new BigDecimal(700.4));

        List<Funcionario> listaFuncionario = new ArrayList<>();
        listaFuncionario.add(funcionario1);
        listaFuncionario.add(funcionario2);
        listaFuncionario.add(funcionario3);
        listaFuncionario.add(funcionario4);
        listaFuncionario.add(funcionario5);
        listaFuncionario.add(funcionario6);
        listaFuncionario.add(funcionario7);
        listaFuncionario.add(funcionario8);
        listaFuncionario.add(funcionario9);
        listaFuncionario.add(funcionario10);

        MyCalculo myCalculo = new MyCalculo();
        myCalculo.custoPorCargo(listaFuncionario);
        myCalculo.custoPorDepartamento(listaFuncionario);

    }


}
